﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Doki_doki_tetris_club
{
    /// <summary>
    /// Lógica de interacción para Video.xaml
    /// </summary>
    public partial class Video : Window
    {
        #region Atributos

        #region Videos
        private readonly List<string> videos = new List<string>
        {
             "Recursos/Videos/JustMonika.mp4"
        };
        #endregion

        #region Fondos
        private readonly List<ImageBrush> fondo = new List<ImageBrush>
        {
            new ImageBrush(new BitmapImage(new Uri("Recursos/Imagenes/Botones/estampadoRosa.jpg",UriKind.Relative)))
        };
        #endregion

        #endregion

        #region Constructor
        public Video()
        {
            InitializeComponent();
            reproductor.Source = new Uri(videos[0], UriKind.Relative);
            button.Background = fondo[0];
        }
        #endregion

        #region Métodos

        #region Video
        private void reproductor_MediaEnded(object sender, RoutedEventArgs e)
        {
            PantallaJuego p = new PantallaJuego(1);
            p.Show();
            this.Close();
        }
        #endregion

        #region Eventos click
        private void Omitir_click(object sender, RoutedEventArgs e)
        {
            PantallaJuego p = new PantallaJuego(1);
            p.Show();
            this.Close();
        }
        #endregion

        #endregion
    }
}
