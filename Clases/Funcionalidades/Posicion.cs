﻿namespace Doki_doki_tetris_club
{
    public class Posicion
    {
        #region Atributos
        public int Fila { get; set; }
        public int Columna { get; set; }
        #endregion

        #region Constructor
        public Posicion(int row, int column)
        {
            Fila = row;
            Columna = column;
        }
        #endregion
    }
}
