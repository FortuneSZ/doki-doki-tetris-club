﻿namespace Doki_doki_tetris_club
{
    //Clase que gestiona la partida
    public class EstadoDeJuego
    {
        #region Atributos
        private Bloque BloqueActual;

        public Bloque VerBloqueActual
        {
            get => BloqueActual;
            private set
            {
                BloqueActual = value;
                BloqueActual.Reiniciar();

                for (int i = 0; i < 2; i++)
                {
                    VerBloqueActual.Mover(1, 0);

                    if (!ElBloqueCabe())
                    {
                        VerBloqueActual.Mover(-1, 0);
                    }
                }
            }
        }

        public GameGrid GameGrid { get; }
        public ColaBloques Cola { get; }
        public bool GameOver { get; private set; }
        public bool Pausa { get; private set; }
        public int Puntuacion { get; private set; }
        public Bloque BloqueGuardado { get; private set; }
        public bool PuedeGuardarse { get; private set; }
        #endregion

        #region Constructor
        public EstadoDeJuego()
        {
            GameGrid = new GameGrid(22, 10);
            Cola = new ColaBloques();
            VerBloqueActual = Cola.Actualizar();
            PuedeGuardarse = true;
            Pausa= false;
        }
        #endregion

        #region Métodos

        #region Colocar
        //Método que coloca el bloque si este no puede moverse más
        private void ColocarBloque()
        {
            foreach (Posicion p in VerBloqueActual.PosicionCasillas())
            {
                GameGrid[p.Fila, p.Columna] = VerBloqueActual.Id;
            }

            Puntuacion += GameGrid.BorrarFilasLLenas();

            if (FinDelJuego())
            {
                GameOver = true;
            }
            else
            {
                VerBloqueActual = Cola.Actualizar();
                PuedeGuardarse = true;
            }
        }
        #endregion

        #region Guardar
        /*Método que guarda un bloque para usarlo posteriormente,en caso de ya
        haber uno guardado,lo intercambia por ese*/
        public void GuardarBloque()
        {
            if (!PuedeGuardarse)
            {
                return;
            }

            if (BloqueGuardado == null)
            {
                BloqueGuardado = VerBloqueActual;
                VerBloqueActual = Cola.Actualizar();
            }
            else
            {
                Bloque tmp = VerBloqueActual;
                VerBloqueActual = BloqueGuardado;
                BloqueGuardado = tmp;
            }

            PuedeGuardarse = false;
        }
        #endregion

        #region Rotar
        //Método que gira el bloque en sentido horario si es posible
        public void RotarBloqueHorario()
        {
            VerBloqueActual.RotarHorario();

            if (!ElBloqueCabe())
            {
                VerBloqueActual.RotarAntiHorario();
            }
        }
        //Método que gira el bloque en sentido antihorario si es posible
        public void RotarBloqueAntihorario()
        {
            VerBloqueActual.RotarAntiHorario();

            if (!ElBloqueCabe())
            {
                VerBloqueActual.RotarHorario();
            }
        }
        #endregion

        #region Mover
        //Método que mueve el bloque hacia la izquierda si es posible
        public void MoverBloqueIzquierda()
        {
            VerBloqueActual.Mover(0, -1);

            if (!ElBloqueCabe())
            {
                VerBloqueActual.Mover(0, 1);
            }
        }
        //Método que mueve el bloque a la derecha si es posible
        public void MoverBloqueDerecha()
        {
            VerBloqueActual.Mover(0, 1);

            if (!ElBloqueCabe())
            {
                VerBloqueActual.Mover(0, -1);
            }
        }

        /*Método que mueve el bloque hacia abajo de forma automática hasta que
        choque con otro*/
        public void MoverBloqueAbajo()
        {
            VerBloqueActual.Mover(1, 0);

            if (!ElBloqueCabe())
            {
                VerBloqueActual.Mover(-1, 0);
                ColocarBloque();
            }
        }
        #endregion

        #region Game Over
        //Método que determina cuando acaba el juego
        private bool FinDelJuego()
        {
            return !(GameGrid.FilaVacia(0) && GameGrid.FilaVacia(1));
        }
        #endregion

        #region Comprobaciones
        //Método que permite saber si el bloque cabe en un espacio
        private bool ElBloqueCabe()
        {
            foreach (Posicion p in VerBloqueActual.PosicionCasillas())
            {
                if (!GameGrid.EstaVacio(p.Fila, p.Columna))
                {
                    return false;
                }
            }

            return true;
        }
   
        /*Método que mide la distancia en casillas que puede caer un bloque
        desde su posición actual*/
        private int TileDropDistance(Posicion p)
        {
            int drop = 0;

            while (GameGrid.EstaVacio(p.Fila + drop + 1, p.Columna))
            {
                drop++;
            }

            return drop;
        }

        //Método que mide cuantas filas puede caer el bloque
        public int BlockDropDistance()
        {
            int drop = GameGrid.Filas;

            foreach (Posicion p in VerBloqueActual.PosicionCasillas())
            {
                drop = System.Math.Min(drop, TileDropDistance(p));
            }

            return drop;
        }
        #endregion

        #region Dejar caer
        //Método que hace caer el bloque
        public void DropBlock()
        {
            VerBloqueActual.Mover(BlockDropDistance(), 0);
            ColocarBloque();
        }
        #endregion

        #region Pausa
        public void Pausar()
        {
            Pausa = true;
        }

        public void DesPausar()
        {
            Pausa = false;
        }

        public void PausaMenu()
        {
            if (Pausa == true)
            {
                DesPausar();
            }
            else
            {
                Pausar();
            }
        }
        #endregion

        #endregion
    }
}
