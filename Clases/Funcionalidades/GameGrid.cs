﻿namespace Doki_doki_tetris_club
{
    //Clase del "grid" o malla donde se realizará el juego
    public class GameGrid
    {
        #region Atributos
        private readonly int[,] grid;
        public int Filas { get; }
        public int Columnas { get; }

        public int this[int f, int c]
        {
            get => grid[f, c];
            set => grid[f, c] = value;
        }
        #endregion

        #region Constructor
        public GameGrid(int f, int c)
        {
            Filas = f;
            Columnas = c;
            grid = new int[f, c];
        }
        #endregion

        #region Métodos

        #region Comprobaciones
        //Método que permite saber si en una casilla hay un bloque
        public bool EstaDentro(int f, int c)
        {
            return f >= 0 && f < Filas && c >= 0 && c < Columnas;
        }

        //Método que permite saber si una casilla está vacía
        public bool EstaVacio(int f, int c)
        {
            return EstaDentro(f, c) && grid[f, c] == 0;
        }

        //Método que permite saber si una fila está llena
        public bool FilaLlena(int f)
        {
            for (int c = 0; c < Columnas; c++)
            {
                if (grid[f, c] == 0)
                {
                    return false;
                }
            }

            return true;
        }

        //Método que permite saber si una fila está vacía
        public bool FilaVacia(int f)
        {
            for (int c = 0; c < Columnas; c++)
            {
                if (grid[f, c] != 0)
                {
                    return false;
                }
            }

            return true;
        }
        #endregion

        #region Borrar
        //Método que permite borrar una fila
        private void BorrarFila(int f)
        {
            for (int c = 0; c < Columnas; c++)
            {
                grid[f, c] = 0;
            }
        }
        
        //Método que borra una fila cuando está llena
        public int BorrarFilasLLenas()
        {
            int cleared = 0;

            for (int f = Filas - 1; f >= 0; f--)
            {
                if (FilaLlena(f))
                {
                    BorrarFila(f);
                    cleared++;
                }
                else if (cleared > 0)
                {
                    BajarFilas(f, cleared);
                }
            }

            return cleared;
        }
        #endregion

        #region Bajar
        //Método que baja las filas superiores a la que hemos borrado
        private void BajarFilas(int f, int numRows)
        {
            for (int c = 0; c < Columnas; c++)
            {
                grid[f + numRows, c] = grid[f, c];
                grid[f, c] = 0;
            }
        }
        #endregion

        #endregion
    }
}
