﻿using System;

namespace Doki_doki_tetris_club
{
    //Clase cola de bloques
    public class ColaBloques
    {
        #region Atributos
        //Atributo que crea un array con un bloque de cada tipo
        private readonly Bloque[] Bloques = new Bloque[]
        {
            new BloqueI(),
            new BloqueJ(),
            new BloqueL(),
            new BloqueO(),
            new BloqueS(),
            new BloqueT(),
            new BloqueZ()
        };

        private readonly Random random = new Random();

        public Bloque SiguienteBloque { get; private set; }
        #endregion

        #region Constructor
        public ColaBloques()
        {
            SiguienteBloque = BloqueAleatorio();
        }
        #endregion

        #region Métodos
        //Método que elije un bloque aleatorio
        private Bloque BloqueAleatorio()
        {
            return Bloques[random.Next(Bloques.Length)];
        }

        //Método que actuliza el bloque
        public Bloque Actualizar()
        {
            Bloque block = SiguienteBloque;

            do
            {
                SiguienteBloque = BloqueAleatorio();
            }
            while (block.Id == SiguienteBloque.Id);

            return block;
        }
        #endregion
    }
}
