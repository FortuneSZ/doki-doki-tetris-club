﻿namespace Doki_doki_tetris_club
{
    //Clase del bloque con forma de J
    public class BloqueJ : Bloque
    {
        #region Atributos
        public override int Id => 2;

        //Atributo que determina la posición inicial del bloque
        protected override Posicion DBInicial => new(0, 3);

        /*Atributo que determina las distintas posiciones del bloque,según su
        estado de rotación*/
        protected override Posicion[][] Casillas => new Posicion[][] {
            new Posicion[] {new(0, 0), new(1, 0), new(1, 1), new(1, 2)},
            new Posicion[] {new(0, 1), new(0, 2), new(1, 1), new(2, 1)},
            new Posicion[] {new(1, 0), new(1, 1), new(1, 2), new(2, 2)},
            new Posicion[] {new(0, 1), new(1, 1), new(2, 1), new(2, 0)}
        };
        #endregion
    }
}
