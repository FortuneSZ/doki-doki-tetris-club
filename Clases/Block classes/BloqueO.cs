﻿namespace Doki_doki_tetris_club
{
    //Clase del bloque con forma de O
    public class BloqueO : Bloque
    {
        #region Atributos
        public override int Id => 4;

        /*Atributo que determina las distintas posiciones del bloque,según su
        estado de rotación*/
        private readonly Posicion[][] tiles = new Posicion[][]
        {
            new Posicion[] { new(0,0), new(0,1), new(1,0), new(1,1) }
        };

        //Atributo que determina la posición inicial del bloque
        protected override Posicion DBInicial => new Posicion(0, 4);

        //Atributo que determina la posición del bloque
        protected override Posicion[][] Casillas => tiles;
    }
    #endregion
}
