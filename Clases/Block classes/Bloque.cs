﻿using System.Collections.Generic;

namespace Doki_doki_tetris_club
{
    //Clase Bloque
    public abstract class Bloque
    {
        #region Atributos
        protected abstract Posicion[][] Casillas { get; }
        protected abstract Posicion DBInicial { get; }
        public abstract int Id { get; }

        private int EstadoRotacion;

        private Posicion DistanciaBorde;
        #endregion

        #region Constructor
        public Bloque()
        {
            DistanciaBorde = new Posicion(DBInicial.Fila, DBInicial.Columna);
        }
        #endregion

        #region Métodos
        //Método que enumera la posición en la que se encuentran los bloques
        public IEnumerable<Posicion> PosicionCasillas()
        {
            foreach (Posicion p in Casillas[EstadoRotacion])
            {
                yield return new Posicion(p.Fila + DistanciaBorde.Fila,
                    p.Columna + DistanciaBorde.Columna);
            }
        }
        
        //Método para girar el bloque en sentido horario
        public void RotarHorario()
        {
            EstadoRotacion = (EstadoRotacion + 1) % Casillas.Length;
        }

        //Método para girar el bloque en sentido antihorario
        public void RotarAntiHorario()
        {
            if (EstadoRotacion == 0)
            {
                EstadoRotacion = Casillas.Length - 1;
            }
            else
            {
                EstadoRotacion--;
            }
        }

        //Método para mover el bloque
        public void Mover(int Filas, int Columnas)
        {
            DistanciaBorde.Fila += Filas;
            DistanciaBorde.Columna += Columnas;
        }

        //Método para reiniciar el bloque
        public void Reiniciar()
        {
            EstadoRotacion = 0;
            DistanciaBorde.Fila = DBInicial.Fila;
            DistanciaBorde.Columna = DBInicial.Columna;
        }
        #endregion
    }
}
