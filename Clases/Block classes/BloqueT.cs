﻿namespace Doki_doki_tetris_club
{
    //Clase del bloque con forma de T
    public class BloqueT : Bloque
    {
        #region Atributos
        public override int Id => 6;

        //Atributo que determina la posición inicial del bloque
        protected override Posicion DBInicial => new(0, 3);

        /*Atributo que determina las distintas posiciones del bloque,según su
        estado de rotación*/
        protected override Posicion[][] Casillas => new Posicion[][] {
            new Posicion[] {new(0,1), new(1,0), new(1,1), new(1,2)},
            new Posicion[] {new(0,1), new(1,1), new(1,2), new(2,1)},
            new Posicion[] {new(1,0), new(1,1), new(1,2), new(2,1)},
            new Posicion[] {new(0,1), new(1,0), new(1,1), new(2,1)}
        };
        #endregion
    }
}
