﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using WpfAnimatedGif;

namespace Doki_doki_tetris_club
{
    /// <summary>
    /// Lógica de interacción para PantallaJuego.xaml
    /// </summary>
    public partial class PantallaJuego : Window
    {
        #region Atributos
        
        #region Imágenes

        #region Imagenes bloques
        //Insertación de las imágenes de los patrones de los bloques
        private readonly List<ImageSource> tileImages = new List<ImageSource>
        {
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques/TileEmpty.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques/BloqueRosa.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques/BloqueAzul.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques/BloqueNaranja.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques/BloqueAmarillo.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques/BloqueVerde.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques/BloqueMorado.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques/BloqueRojo.png", UriKind.Relative))
        };
        #endregion

        #region Imagenes bloques guardados
        //Insertación de las imágenes de los bloques
        private readonly List<ImageSource> blockImages = new List<ImageSource>
        {
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques_Hold/Block-Empty.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques_Hold/Block-I.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques_Hold/Block-J.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques_Hold/Block-L.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques_Hold/Block-O.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques_Hold/Block-S.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques_Hold/Block-T.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Bloques_Hold/Block-Z.png", UriKind.Relative))
        };
        #endregion

        #region Stickers
        //Imágenes de los stickers que aparecen en el menú de pausa y de game over

        #region Monika
        private readonly List<ImageSource> Monistickers = new List<ImageSource>
        {
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Monika/Monika_1.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Monika/Monika_2.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Monika/Monika_3.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Monika/Monika_4.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Monika/Monika_5.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Monika/Monika_6.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Monika/Monika_7.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Monika/Monika_8.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Monika/Monika_9.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Monika/Monika_10.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Monika/Monika_11.png", UriKind.Relative))
        };
        #endregion

        #region Sayori
        private readonly List<ImageSource> Sayostickers = new List<ImageSource>
        {
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Sayori/Sayori_1.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Sayori/Sayori_2.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Sayori/Sayori_3.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Sayori/Sayori_4.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Sayori/Sayori_5.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Sayori/Sayori_6.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Sayori/Sayori_7.png", UriKind.Relative))
        };
        #endregion

        #region Yuri
        private readonly List<ImageSource> Yurstickers = new List<ImageSource>
        {
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Yuri/Yuri_1.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Yuri/Yuri_2.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Yuri/Yuri_3.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Yuri/Yuri_4.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Yuri/Yuri_5.png", UriKind.Relative))
        };
        #endregion

        #region Natsuki
        private readonly List<ImageSource> Natsustickers = new List<ImageSource>
        {
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Natsuki/Natsuki_1.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Natsuki/Natsuki_2.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Natsuki/Natsuki_3.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Natsuki/Natsuki_4.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Natsuki/Natsuki_5.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Natsuki/Natsuki_6.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Stickers/Natsuki/Natsuki_7.png", UriKind.Relative)),
        };
        #endregion

        #endregion

        #region Sprites

        #region Sayori
        //Sprites del personaje de Sayori
        private readonly List<ImageSource> SayoriSprite = new List<ImageSource>
        {
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori1.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori3.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori4.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori5.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori6.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori7.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori8.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori9.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori10.png", UriKind.Relative))
        };

        private readonly List<ImageSource> SayoriAct2Sprite = new List<ImageSource>
        {
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_1.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_2.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_3.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_4.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_5.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_6.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_7.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_8.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_9.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_10.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_11.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_12.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_13.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_14.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_15.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_16.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_17.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayori2_18.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Sayori/Sayonara.png", UriKind.Relative))
        };
        #endregion

        #region Yuri
        private readonly List<ImageSource> YuriSprite = new List<ImageSource>
        {
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri1.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri2.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri3.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri4.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri5.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri6.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri7.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri8.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri9.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri10.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri11.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri12.png", UriKind.Relative)),
        };

        private readonly List<ImageSource> YuriAct2Sprite = new List<ImageSource>
        {
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri2_1.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri2_2.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri2_3.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri2_4.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri2_5.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri2_6.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Yuri/Yuri2_7.png", UriKind.Relative)),
        };

        private readonly Uri YuriGif = new Uri("Recursos/Imagenes/Sprites/Yuri/Yuricide.gif", UriKind.Relative);
        #endregion

        #region Natsuki
        private readonly List<ImageSource> NatsukiSprite = new List<ImageSource>
        {
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki1.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki2.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki3.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki4.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki5.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki6.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki7.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki8.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki9.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki10.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki11.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki12.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki13.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki14.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki15.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki16.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki17.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki18.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki19.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki20.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki21.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki22.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki23.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki24.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki25.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki26.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki27.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki28.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki29.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki30.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki31.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki32.png", UriKind.Relative))
        };

        private readonly List<ImageSource> NatsukiAct2Sprite = new List<ImageSource>
        {
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki2_1.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki2_2.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki2_3.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki2_4.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki2_5.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki2_6.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki2_7.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Natsuki/Natsuki2_8.png", UriKind.Relative))
        };

        #endregion

        #region Monika
        private readonly List<ImageSource> MonikaSprite = new List<ImageSource>
        {
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika1.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika2.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika3.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika4.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika5.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika6.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika7.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika8.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika9.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika10.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika11.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika12.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika13.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika14.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika15.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika16.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika17.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika18.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika19.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika20.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika21.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika22.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika23.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika24.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika25.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika26.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika27.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Sprites/Monika/Monika28.png", UriKind.Relative)),
        };
        #endregion

        #endregion

        #region Fondos
        private readonly List<ImageSource> Fondos = new List<ImageSource>
        {
            new BitmapImage(new Uri("Recursos/Imagenes/Fondos/sayori_bedroom.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Fondos/club.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Fondos/FondoNegro.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Fondos/kitchen.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Fondos/Monika_Room.png", UriKind.Relative)),
            new BitmapImage(new Uri("Recursos/Imagenes/Fondos/Monika.png", UriKind.Relative)),
        };
        #endregion

        #endregion

        #region Canciones

        #region Struck canciones
        struct Canciones
        {
            public string Nombre;
            public string Ubicacion;
            public int Duracion;
        }
        #endregion

        #region Nombre canciones
        private readonly string[] nombreCanciones = new string[]
        {
            "Candy hearts",
            "Candy heartz",
            "Okay, Everyone! (Sayori)",
            "My Confession",
            "Sayo-nara",
            "Okay, Everyone! (Yuri)",
            "My Feelings",
            "My Dark Confession",
            "Okay, Everyone! (Natsuki)",
            "Play With Me",
            "Play With M3",
            "Play With Mee",
            "Play With Meee",
            "eM htiW yalP",
            "Okay, Everyone! (Monika)",
            "Just Monika",
            "I Still Love You",
            "Your Reality"
        };

        #endregion

        #region Ubicación canciones
        private readonly string[] canciones = new string[]
        {
            "Recursos/Ost/InfiniteMode.wav",
            "Recursos/Ost/GameOver.wav",
            "Recursos/Ost/Okay, Everyone! (Sayori).wav",
            "Recursos/Ost/My Confession.wav",
            "Recursos/Ost/Sayo-nara.wav",
            "Recursos/Ost/Okay, Everyone! (Yuri).wav",
            "Recursos/Ost/My Feelings.wav",
            "Recursos/Ost/My Dark Confession.wav",
            "Recursos/Ost/Okay, Everyone! (Natsuki).wav",
            "Recursos/Ost/Play With Me.wav",
            "Recursos/Ost/Play With M3.wav",
            "Recursos/Ost/Play With Mee.wav",
            "Recursos/Ost/Play With Meee.wav",
            "Recursos/Ost/eM htiW yalP.wav",
            "Recursos/Ost/Okay, Everyone! (Monika).wav",
            "Recursos/Ost/Just Monika.wav",
            "Recursos/Ost/I Still Love You.wav",
            "Recursos/Ost/Your Reality.wav",

        };
        #endregion

        #region Duración canciones
        private readonly int[] DuracionCanciones = new int[]
        {
            34241448,
            12986828,
            18664388,
            35821892,
            30215664,
            18664388,
            23764604,
            30690468,
            18664388,
            33311908,
            26169820,
            69539492,
            10502144,
            8702468,
            18804820,
            38869092,
            31472884,
            34881204
        };
        #endregion

        #region Lista canciones
        private List<Canciones> songs = new List<Canciones>();
        #endregion

        #endregion

        #region Temporizadores
        DispatcherTimer tiempo = new DispatcherTimer();
        DispatcherTimer tiempoSprite = new DispatcherTimer();
        #endregion

        #region Música
        private WaveStream streamTetrisMusic;
        private WaveOut PlayTetrisMusic;
        private int SelectedSong;
        #endregion

        #region Imagen
        private readonly Image[,] imageControls;
        #endregion

        #region Delay
        private readonly int maxDelay = 1000;
        private readonly int minDelay = 75;
        private readonly int delayDecrease = 25;
        #endregion

        #region GameState
        private EstadoDeJuego gameState = new EstadoDeJuego();
        #endregion

        #region Level
        private int Level;
        private bool LevelSelect;
        #endregion

        #endregion

        #region Constructor
        public PantallaJuego(int level)
        {
            InitializeComponent();
            imageControls = SetupGameCanvas(gameState.GameGrid);
            CrearCanciones();
            SetupTimers();
            Level = level;
        }

        public PantallaJuego(int level,bool select)
        {
            InitializeComponent();
            imageControls = SetupGameCanvas(gameState.GameGrid);
            CrearCanciones();
            SetupTimers();
            Level = level;
            LevelSelect = true;
        }
        #endregion

        #region Métodos

        #region Marco
        //Método que pone a punto el marco de juego
        private Image[,] SetupGameCanvas(GameGrid grid)
        {
            Image[,] imageControls = new Image[grid.Filas, grid.Columnas];
            int cellSize = 25;

            for (int r = 0; r < grid.Filas; r++)
            {
                for (int c = 0; c < grid.Columnas; c++)
                {
                    Image imageControl = new Image
                    {
                        Width = cellSize,
                        Height = cellSize
                    };

                    Canvas.SetTop(imageControl, (r - 2) * cellSize + 10);
                    Canvas.SetLeft(imageControl, c * cellSize);
                    GameCanvas.Children.Add(imageControl);
                    imageControls[r, c] = imageControl;
                }
            }

            return imageControls;
        }
        #endregion

        #region Dibujo
        //Método que dibuja el grid
        private void DibujarGrid(GameGrid grid)
        {
            for (int r = 0; r < grid.Filas; r++)
            {
                for (int c = 0; c < grid.Columnas; c++)
                {
                    int id = grid[r, c];
                    imageControls[r, c].Opacity = 1;
                    imageControls[r, c].Source = tileImages[id];
                }
            }
        }

        //Método que dibujar el bloque
        private void DibujarBloque(Bloque block)
        {
            foreach (Posicion p in block.PosicionCasillas())
            {
                imageControls[p.Fila, p.Columna].Opacity = 1;
                imageControls[p.Fila, p.Columna].Source = tileImages[block.Id];
            }
        }

        //Método que dibuja el siguiente bloque
        private void DibujarSiguienteBloque(ColaBloques blockQueue)
        {
            Bloque next = blockQueue.SiguienteBloque;
            NextImage.Source = blockImages[next.Id];
        }

        //Método que dibuja el bloque guardado
        private void DibujarBloqueGuardado(Bloque heldBlock)
        {
            if (heldBlock == null)
            {
                HoldImage.Source = blockImages[0];
            }
            else
            {
                HoldImage.Source = blockImages[heldBlock.Id];
            }
        }

        //Método que dibuja la silueta del bloque donde va a caer
        private void DibujarBloqueFantasma(Bloque block)
        {
            int dropDistance = gameState.BlockDropDistance();

            foreach (Posicion p in block.PosicionCasillas())
            {
                imageControls[p.Fila + dropDistance, p.Columna].Opacity = 0.25;
                imageControls[p.Fila + dropDistance, p.Columna].Source = tileImages[block.Id];
            }
        }

        //Método dibujar que llama a los otros métodos de dibujo
        private void Dibujar(EstadoDeJuego gameState)
        {
            DibujarGrid(gameState.GameGrid);
            DibujarBloqueFantasma(gameState.VerBloqueActual);
            DibujarBloque(gameState.VerBloqueActual);
            DibujarSiguienteBloque(gameState.Cola);
            DibujarBloqueGuardado(gameState.BloqueGuardado);
            ScoreText.Text = $"Puntuación: {gameState.Puntuacion}";
        }
        #endregion

        #region Game loop
        //Tarea que se encarga del loop jugable
        private async Task GameLoop()
        {
            bool Happened_1 = false, Happened_2 = false,
                Happened_3 = false, Happened_4 = false;

            if (Level== 0) 
            {
                SeleccionarMusica(0);
                while (!gameState.GameOver)
                {
                    int delay = SetDelay();
                    await Task.Delay(delay);
                    Gameplay();
                    thetimer.Text = "Modo infinito";
                }
                GameOver();
            }

            if (Level== 1)
            {
                SeleccionarMusica(0);
                character.Source = SayoriSprite[0];
                while (!gameState.GameOver && gameState.Puntuacion < 35)
                {
                    int delay = SetDelay();
                    await Task.Delay(delay);
                    Gameplay();
                    thetimer.Text = "Nivel 1";
                }

                if (gameState.GameOver) 
                {
                    GameOver();
                }

                if(!gameState.GameOver)
                {
                    Victoria();
                }               
            }

            if (Level == 2) 
            {
                SeleccionarMusica(0);
                character.Source = YuriSprite[0];
                while (!gameState.GameOver && gameState.Puntuacion < 50)
                {
                    int delay = SetDelay();
                    await Task.Delay(delay);
                    Gameplay();
                    thetimer.Text = "Nivel 2";
                }

                if (gameState.GameOver)
                {
                    GameOver();
                }

                if (!gameState.GameOver)
                {
                    Victoria();
                }
            }

            if (Level == 3) 
            {
                SeleccionarMusica(0);
                character.Source = NatsukiSprite[0];
                while (!gameState.GameOver && gameState.Puntuacion < 75)
                {
                    int delay = SetDelay();
                    await Task.Delay(delay);
                    Gameplay();
                    thetimer.Text = "Nivel 3";
                }

                if (gameState.GameOver)
                {
                    GameOver();
                }

                if (!gameState.GameOver)
                {
                    Victoria();
                }
            }

            if (Level == 4)
            {
                SeleccionarMusica(0);
                character.Source = MonikaSprite[0];
                while (!gameState.GameOver && gameState.Puntuacion < 100)
                {

                    int delay = SetDelay();
                    await Task.Delay(delay);
                    Gameplay();
                    thetimer.Text = "Nivel 4";
                }

                if (gameState.GameOver)
                {
                    GameOver();
                }

                if (!gameState.GameOver)
                {
                    Victoria();
                }
            }

            if (Level== 5) 
            {
                SeleccionarMusica(2);
                GameBackground.ImageSource = Fondos[0];
                character.Source = SayoriAct2Sprite[0];
                while (!gameState.GameOver && gameState.Puntuacion < 75)
                {
                    if (gameState.Puntuacion >= 25 && Happened_1 == false)
                    {
                        Happened_1 = true;
                        PlayTetrisMusic.Stop();
                        SeleccionarMusica(3);
                    }

                    else if (gameState.Puntuacion >= 50 && Happened_2 == false)
                    {
                        Happened_2 = true;
                        PlayTetrisMusic.Stop();
                        SeleccionarMusica(4);
                    }
                    int delay = SetDelay();
                    await Task.Delay(delay);
                    Gameplay();
                    thetimer.Text = "Nivel 5";
                }

                if (gameState.GameOver)
                {
                    GameOver();
                }
                
                if (!gameState.GameOver)
                {
                    Victoria();
                }
            }

            if (Level == 6)
            {
                SeleccionarMusica(5);
                GameBackground.ImageSource = Fondos[1];
                character.Source = YuriSprite[0];
                while (!gameState.GameOver && gameState.Puntuacion < 70)
                {
                    if (gameState.Puntuacion >= 35 && Happened_1 == false)
                    {
                        Happened_1 = true;
                        PlayTetrisMusic.Stop();
                        SeleccionarMusica(6);
                        character.Source = YuriAct2Sprite[0];
                    }
                    int delay = SetDelay();
                    await Task.Delay(delay);
                    Gameplay();
                    thetimer.Text = "Nivel 6";
                }

                if (gameState.GameOver)
                {
                    GameOver();
                }

                if (!gameState.GameOver)
                {
                    Victoria();
                }
            }

            if (Level == 7)
            {
                SeleccionarMusica(8);
                GameBackground.ImageSource = Fondos[3];
                character.Source = NatsukiSprite[0];
                while (!gameState.GameOver && gameState.Puntuacion < 100)
                {
                    if (gameState.Puntuacion >= 10 && Happened_1 == false)
                    {
                        Happened_1 = true;
                        PlayTetrisMusic.Stop();
                        SeleccionarMusica(9);
                    }

                    if (gameState.Puntuacion >= 25 && Happened_2 == false)
                    {
                        Happened_2 = true;
                        PlayTetrisMusic.Stop();
                        SeleccionarMusica(10);
                        character.Source = NatsukiAct2Sprite[0];
                    }

                    if (gameState.Puntuacion >= 50 && Happened_3 == false)
                    {
                        Happened_3 = true;
                        PlayTetrisMusic.Stop();
                        SeleccionarMusica(11);
                        character.Source = NatsukiAct2Sprite[5];
                    }

                    if (gameState.Puntuacion >= 75 && Happened_4 == false)
                    {
                        Happened_4 = true;
                        PlayTetrisMusic.Stop();
                        SeleccionarMusica(12);
                        character.Source = NatsukiAct2Sprite[6];
                    }
                    int delay = SetDelay();
                    await Task.Delay(delay);
                    Gameplay();
                    thetimer.Text = "Nivel 7";
                }

                if (gameState.GameOver)
                {
                    GameOver();
                }

                if (!gameState.GameOver)
                {
                    Victoria();
                }
            }

            if (Level == 8)
            {
                SeleccionarMusica(14);
                GameBackground.ImageSource = Fondos[4];
                character.Source = MonikaSprite[0];
                while (!gameState.GameOver && gameState.Puntuacion < 125)
                {
                    if (gameState.Puntuacion >= 25 && Happened_1 == false)
                    {
                        Happened_1 = true;
                        PlayTetrisMusic.Stop();
                        SeleccionarMusica(15);
                        GameBackground.ImageSource = Fondos[5];
                        character.Source = SayoriAct2Sprite[18];
                    }

                    if (gameState.Puntuacion >= 50 && Happened_2 == false)
                    {
                        Happened_2 = true;
                        character.Source = YuriAct2Sprite[0];
                    }

                    if (gameState.Puntuacion >= 75 && Happened_3 == false)
                    {
                        Happened_3 = true;
                        character.Source = NatsukiAct2Sprite[0];
                    }

                    if (gameState.Puntuacion >= 100 && Happened_4 == false)
                    {
                        Happened_4 = true;
                        PlayTetrisMusic.Stop();
                        SeleccionarMusica(16);
                        GameBackground.ImageSource = Fondos[4];
                        character.Source = MonikaSprite[0];
                    }
                    int delay = SetDelay();
                    await Task.Delay(delay);
                    Gameplay();
                    thetimer.Text = "Nivel 8";
                }

                if (gameState.GameOver)
                {
                    GameOver();
                }

                if (!gameState.GameOver)
                {
                    Victoria();
                }
            }
        }
        #endregion

        #region Gameplay
        private void Gameplay()
        {
            Dibujar(gameState);
            gameState.MoverBloqueAbajo();
            Dibujar(gameState);
        }
        #endregion

        #region Delay

        private int SetDelay()
        {
            int delay = Math.Max(minDelay, maxDelay);
            if (Level == 0)
            {
                delay = Math.Max(minDelay, maxDelay - (gameState.Puntuacion * delayDecrease));
            }

            if (Level == 1)
            {
                if (gameState.Puntuacion > 20)
                {
                    delay = Math.Max(minDelay, maxDelay - (10 * delayDecrease));
                }
            }

            if (Level == 2)
            {
                if (gameState.Puntuacion < 15)
                {
                    delay = Math.Max(minDelay, maxDelay - (5 * delayDecrease));
                }
                else if (gameState.Puntuacion < 30)
                {
                    delay = Math.Max(minDelay, maxDelay - (10 * delayDecrease));
                }
                else
                {
                    delay = Math.Max(minDelay, maxDelay - (15 * delayDecrease));
                }
            }

            if (Level == 3)
            {
                if (gameState.Puntuacion < 25)
                {
                    delay = Math.Max(minDelay, maxDelay - (10 * delayDecrease));
                }
                else if (gameState.Puntuacion < 50)
                {
                    delay = Math.Max(minDelay, maxDelay - (15 * delayDecrease));
                }
                else
                {
                    delay = Math.Max(minDelay, maxDelay - (20 * delayDecrease));
                }
            }

            if (Level == 4)
            {
                if (gameState.Puntuacion < 25)
                {
                    delay = Math.Max(minDelay, maxDelay - (10 * delayDecrease));
                }
                else if (gameState.Puntuacion < 50)
                {
                    delay = Math.Max(minDelay, maxDelay - (15 * delayDecrease));
                }
                else if (gameState.Puntuacion < 75)
                {
                    delay = Math.Max(minDelay, maxDelay - (20 * delayDecrease));
                }
                else
                {
                    delay = Math.Max(minDelay, maxDelay - (25 * delayDecrease));
                }
            }

            if (Level == 5)
            {
                if (gameState.Puntuacion < 25)
                {
                    delay = Math.Max(minDelay, maxDelay - (10 * delayDecrease));
                }
                else
                {
                    delay = Math.Max(minDelay, maxDelay - (20 * delayDecrease));
                }
            }

            if (Level == 6)
            {
                if (gameState.Puntuacion < 35)
                {
                    delay = Math.Max(minDelay, maxDelay - (15 * delayDecrease));
                }
                else
                {
                    delay = Math.Max(minDelay, maxDelay - (25 * delayDecrease));
                }
            }

            if (Level == 7)
            {
                if (gameState.Puntuacion < 25)
                {
                    delay = Math.Max(minDelay, maxDelay - (15 * delayDecrease));
                }
                else
                {
                    delay = Math.Max(minDelay, maxDelay - (20 * delayDecrease));
                }
            }

            if (Level == 8)
            {
                int[] HardDelay = {10,15,20,25,30};
                Random r= new Random();
                delay = Math.Max(minDelay, maxDelay - (HardDelay[r.Next(5)] * delayDecrease));
            }
            return delay;
        }
        #endregion

        #region Game Over

        //Método que muestra la pantalla de Game Over 
        private void GameOver()
        {
            PlayTetrisMusic.Stop();

            if (Level == 0)
            {
                SeleccionarMusica(1);           
            }

            if (Level != 0)
            {
                tiempoSprite.Stop();
            }

            if (Level == 1 || Level == 5)
            {
                GameOverImg.Source = Sayostickers[0];
                if (Level == 1)
                {
                    SeleccionarMusica(1);
                }
                else
                {
                    SeleccionarMusica(4);
                }
            }

            if (Level == 2 || Level == 6)
            {
                GameOverImg.Source = Yurstickers[0];
                if (Level == 2)
                {
                    SeleccionarMusica(1);
                }
                else
                {
                    SeleccionarMusica(7);
                }
            }

            if (Level == 3 || Level == 7)
            {
                GameOverImg.Source = Natsustickers[0];
                if (Level == 3)
                {
                    SeleccionarMusica(1);
                }
                else
                {
                    SeleccionarMusica(13);
                }
            }

            if (Level == 4 || Level == 8)
            {
                GameOverImg.Source = Monistickers[4];
                if (Level == 4)
                {
                    SeleccionarMusica(1);
                }
                else
                {
                    SeleccionarMusica(17);
                }
            }
            ShowGameOver();
        }

        private void ShowGameOver()
        {
            GameOverMenu.Visibility = Visibility.Visible;
            Controles.Visibility = Visibility.Hidden;
            FinalScoreText.Text = $"Score: {gameState.Puntuacion}";
        }
        #endregion

        #region Victoria
        private void Victoria()
        {
            tiempoSprite.Stop();

            if (LevelSelect == true)
            {
                NextLevel.Visibility = Visibility.Collapsed;
            }

            if (Level == 1)
            {
                VictoryImage.Source = Sayostickers[5];
            }

            if (Level == 2) 
            {
                VictoryImage.Source = Yurstickers[3];
            }

            if (Level == 3)
            {
                
                VictoryImage.Source = Natsustickers[1];
            }

            if (Level == 4)
            {
                VictoryImage.Source = Monistickers[1];
            }

            if (Level > 4)
            {
                VictoryText.Text = "¿Victoria?";
            }

            if (Level == 5)
            {
                VictoryImage.Source = Monistickers[7];
            }

            if (Level == 6)
            {
                VictoryImage.Source = Monistickers[7];
                GameBackground.ImageSource = Fondos[2];
                Mediacharacter.Source = YuriGif;
            }

            if (Level == 7)
            {
                VictoryImage.Source = Monistickers[7];
                character.Source = NatsukiAct2Sprite[7];
            }

            if (Level == 8)
            {
                NextLevel.Visibility= Visibility.Collapsed;
                VictoryImage.Source = Monistickers[6];
            }

            if (Level != 6)
            {
                VictoryMenu.Visibility = Visibility.Visible;
            }

            Unlocks();
        }
        #endregion

        #region Teclas
        //Método que adjudica las distintas acciones a teclas
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (gameState.GameOver)
            {
                return;
            }

            switch (e.Key)
            {
                case Key.Left:
                    gameState.MoverBloqueIzquierda();
                    break;

                case Key.Right:
                    gameState.MoverBloqueDerecha();
                    break;

                case Key.Down:
                    gameState.MoverBloqueAbajo();
                    break;

                case Key.Up:
                case Key.D:
                    gameState.RotarBloqueHorario();
                    break;

                case Key.Z:
                case Key.A:
                    gameState.RotarBloqueAntihorario();
                    break;

                case Key.C:
                    gameState.GuardarBloque();
                    break;

                case Key.Space:
                    gameState.DropBlock();
                    break;

                case Key.P:
                    MenuControles();
                    gameState.PausaMenu();
                    break;

                default:
                    return;
            }

            Dibujar(gameState);
        }
        #endregion

        #region On load
        //método que se inicia cuando el marco carga
        private async void GameCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            await GameLoop();
        }
        #endregion

        #region Eventos click
        /*Método que se ejecuta cuando el usuario presiona el botón de
        jugar de nuevo*/
        private async void PlayAgain_Click(object sender, RoutedEventArgs e)
        {
            tiempoSprite.Start();
            PlayTetrisMusic.Stop();
            gameState = new EstadoDeJuego();
            GameOverMenu.Visibility = Visibility.Hidden;
            await GameLoop();
        }

        private async void VolverMenu_click(object sender, RoutedEventArgs e)
        {
            PlayTetrisMusic.Stop();
            MainWindow m = new MainWindow();
            m.Show();
            this.Close();
        }

        private void NextLevel_click(object sender, RoutedEventArgs e)
        {
            PlayTetrisMusic.Stop();
            Level += 1;
            PantallaJuego p = new PantallaJuego(Level++);
            p.Show();
            this.Close();
        }
        #endregion

        #region Pausa
        /*Método que pausa el juego*/
        private async void MenuControles()
        {
            if (gameState.Pausa == true)
            {
                Controles.Visibility = Visibility.Hidden;
            }
            else
            {
                Random doki = new Random();
                if (Level == 0 || Level == 4 || Level == 8)
                {
                    notpause.Source = Monistickers[doki.Next(Monistickers.Count)];
                }

                if (Level == 1 || Level == 5)
                {
                    notpause.Source = Sayostickers[doki.Next(Sayostickers.Count)];
                }

                if (Level == 2 || Level == 6)
                {
                    notpause.Source = Yurstickers[doki.Next(Yurstickers.Count)];
                }

                if (Level == 3 || Level == 7)
                {
                    notpause.Source = Natsustickers[doki.Next(Natsustickers.Count)];
                }
                if (Level == 4 || Level == 8)
                {
                    notpause.Source = Monistickers[doki.Next(Monistickers.Count)];
                }
                Controles.Visibility = Visibility.Visible;
            }

        }
        #endregion

        #region Música

        private void CrearCanciones()
        {
            for (int i = 0; i < canciones.Count(); i++) 
            {
                Canciones c = new Canciones();
                c.Nombre = nombreCanciones[i];
                c.Ubicacion = canciones[i];
                c.Duracion = DuracionCanciones[i];
                songs.Add(c);
            }
        }
        private void reiniciarMusica(int SelectedSong)
        {
            if (streamTetrisMusic.Position == DuracionCanciones[SelectedSong])
            {
                PlayTetrisMusic.Stop();
                streamTetrisMusic.Position = 0;
                PlayTetrisMusic.Play();
            }
        }

        private void SeleccionarMusica(int i)
        {
            streamTetrisMusic = new WaveFileReader(songs[i].Ubicacion);
            PlayTetrisMusic = new WaveOut();
            PlayTetrisMusic.Init(streamTetrisMusic);
            PlayTetrisMusic.Play();
            SelectedSong = i;
        }
        #endregion

        #region Temporizadores
        private void SetupTimers()
        {
            tiempo.Interval = TimeSpan.FromMilliseconds(1);
            tiempo.Start();
            tiempo.Tick += Timer_Tick;

            tiempoSprite.Interval= TimeSpan.FromSeconds(5);
            tiempoSprite.Start();
            tiempoSprite.Tick += SpriteTimer_Tick;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            ImageVisibility();
            reiniciarMusica(SelectedSong);
        }

        private void SpriteTimer_Tick(object sender, EventArgs e)
        {
            Random doki= new Random();
            if (Level == 1)
            {
                character.Source = SayoriSprite[doki.Next(SayoriSprite.Count)];
            }

            if (Level == 2)
            {
                character.Source = YuriSprite[doki.Next(YuriSprite.Count)];
            }

            if (Level == 3) 
            {
                character.Source = NatsukiSprite[doki.Next(NatsukiSprite.Count)];
            }

            if (Level == 4) 
            {
                character.Source = MonikaSprite[doki.Next(MonikaSprite.Count)];
            }

            if (Level == 5)
            {
                if (gameState.Puntuacion < 25)
                {
                    character.Source = SayoriAct2Sprite[doki.Next(10)];
                }
                else if (gameState.Puntuacion < 50)
                {
                    character.Source = SayoriAct2Sprite[doki.Next(8)+10];
                }
                else
                {
                    character.Source = SayoriAct2Sprite[18];
                }
            }

            if (Level == 6)
            {
                if (gameState.Puntuacion < 35)
                {
                    character.Source = YuriSprite[doki.Next(YuriSprite.Count)];
                }
                else
                {
                    character.Source = YuriAct2Sprite[doki.Next(YuriAct2Sprite.Count)];
                }
            }

            if (Level == 7)
            {
                if (gameState.Puntuacion < 25)
                {
                    character.Source = NatsukiSprite[doki.Next(NatsukiSprite.Count)];
                }
                else if (gameState.Puntuacion < 50)
                {
                    character.Source = NatsukiAct2Sprite[doki.Next(5)];
                }
            }

            if (Level == 8)
            {
                if (gameState.Puntuacion < 25)
                {
                    character.Source = MonikaSprite[doki.Next(MonikaSprite.Count)];
                }

                else if (gameState.Puntuacion < 50)
                {
                    character.Source = SayoriAct2Sprite[18];
                }

                else if (gameState.Puntuacion < 75)
                {
                    character.Source = YuriAct2Sprite[doki.Next(YuriAct2Sprite.Count)];
                }

                else if (gameState.Puntuacion < 100)
                {
                    character.Source = NatsukiAct2Sprite[doki.Next(NatsukiAct2Sprite.Count)];
                }

                else
                {
                    character.Source = MonikaSprite[doki.Next(MonikaSprite.Count)];
                }
            }
        }
        #endregion

        #region Imágenes
        private void ImageVisibility()
        {
            if (this.Width >= 1100 || this.WindowState == WindowState.Maximized)
            {
                character.Visibility = Visibility.Visible;
            }
            else
            {
                character.Visibility = Visibility.Hidden;
            }
        }
        #endregion

        #region Eventos Media
        private void YuriMedia_ended(object sender, RoutedEventArgs e)
        {
            VictoryMenu.Visibility = Visibility.Visible;
        }
        #endregion

        #region Lectura y Escritura
        public void Unlocks()
        {
            string Desbloqueos = @"../../../Recursos/Ficheros/Niveles.txt";
            string WDesbloqueos = @"../../../Recursos/Ficheros/Niveles.txt";
            string Partida = @"../../../Recursos/Ficheros/Continuar.txt";
            string linea;
            string nivel = "Level";
            nivel += Level;
            bool found = false;

            if (File.Exists(Desbloqueos) && LevelSelect != true)
            {
                using (StreamReader RUnlock = new StreamReader(Desbloqueos))
                {
                    while ((linea = RUnlock.ReadLine()) != null)
                    {
                        if (linea.Contains(nivel))
                        {
                            found = true;
                        }
                    }
                    RUnlock.Close();
                }
                using (StreamWriter WUnlock = new StreamWriter(WDesbloqueos, true))
                {
                    if (!found)
                    {
                        WUnlock.WriteLine(nivel);
                    }
                }

                using (StreamWriter WPartida = new StreamWriter(Partida))
                {
                    if (!found)
                    {
                        WPartida.WriteLine(nivel);
                    }
                }
            }
        }
        #endregion

        #endregion
    }
}
