﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Doki_doki_tetris_club
{
    /// <summary>
    /// Lógica de interacción para SelectorNiveles.xaml
    /// </summary>
    public partial class SelectorNiveles : Window
    {
        #region Atributos

        #region Fondos
        private readonly List<ImageBrush> fondoNiveles = new List<ImageBrush>
        {
            new ImageBrush(new BitmapImage(new Uri("Recursos/Imagenes/Botones/Sayori1.png",UriKind.Relative))),
            new ImageBrush(new BitmapImage(new Uri("Recursos/Imagenes/Botones/Yuri1.png",UriKind.Relative))),
            new ImageBrush(new BitmapImage(new Uri("Recursos/Imagenes/Botones/Natsuki1.png",UriKind.Relative))),
            new ImageBrush(new BitmapImage(new Uri("Recursos/Imagenes/Botones/Monika1.png",UriKind.Relative))),
            new ImageBrush(new BitmapImage(new Uri("Recursos/Imagenes/Botones/Sayori2.png",UriKind.Relative))),
            new ImageBrush(new BitmapImage(new Uri("Recursos/Imagenes/Botones/Yuri2.png",UriKind.Relative))),
            new ImageBrush(new BitmapImage(new Uri("Recursos/Imagenes/Botones/Natsuki2.png",UriKind.Relative))),
            new ImageBrush(new BitmapImage(new Uri("Recursos/Imagenes/Botones/Monika2.png",UriKind.Relative))),
            new ImageBrush(new BitmapImage(new Uri("Recursos/Imagenes/Botones/Locked.png",UriKind.Relative)))
        };
        #endregion

        #region Desbloqueos

        private bool[] Desbloqueado = { false, false, false, false, false, false, false, false };

        #endregion

        #endregion

        #region Constructor
        public SelectorNiveles()
        {
            InitializeComponent();
            level1.Background = fondoNiveles[0];
            level2.Background = fondoNiveles[1];
            level3.Background = fondoNiveles[2];
            level4.Background = fondoNiveles[3];
            level5.Background = fondoNiveles[4];
            level6.Background = fondoNiveles[5];
            level7.Background = fondoNiveles[6];
            level8.Background = fondoNiveles[7];
            Unlock();
            Comprobar();
        }
        #endregion

        #region Métodos

        #region Empezar nivel

        private void EmpezarNivel(int nivel)
        {
            PantallaJuego p = new PantallaJuego(nivel,true);
            p.Show();
            this.Close();
        }

        #endregion

        #region Niveles

        private void Level(object sender, RoutedEventArgs e)
        {
            if (sender == level1)
            {
                EmpezarNivel(1);
            }
            if (sender == level2)
            {
                EmpezarNivel(2);
            }
            if (sender == level3)
            {
                EmpezarNivel(3);
            }
            if (sender == level4)
            {
                EmpezarNivel(4);
            }
            if (sender == level5)
            {
                EmpezarNivel(5);
            }
            if (sender == level6)
            {
                EmpezarNivel(6);
            }
            if (sender == level7)
            {
                EmpezarNivel(7);
            }
            if (sender == level8)
            {
                EmpezarNivel(8);
            }
        }
        #endregion

        #region Lectura

        public void Unlock()
        {
            string Desbloqueos = @"../../../Recursos/Ficheros/Niveles.txt";
            int nivel = 1;
            string linea;

            if (File.Exists(Desbloqueos))
            {
                using (StreamReader RUnlock = new StreamReader(Desbloqueos))
                {
                    while ((linea = RUnlock.ReadLine()) != null)
                    {
                        if (linea.Contains("Level" + nivel))
                        {
                            Desbloqueado[nivel - 1] = true;
                        }
                        nivel++;
                    }
                }
            }
        }

        #endregion

        #region Comprobaciones

        public void Comprobar()
        {
            for (int i = 0; i < 8; i++) 
            {
                if (Desbloqueado[i] == false) 
                {
                    switch(i)
                    {
                        case 0:
                            level1.Click -= Level;
                            level1.Background = fondoNiveles[8];
                            break;

                        case 1:
                            level2.Click -= Level;
                            level2.Background = fondoNiveles[8];
                            break;

                        case 2:
                            level3.Click -= Level;
                            level3.Background = fondoNiveles[8];
                            break;

                        case 3:
                            level4.Click -= Level;
                            level4.Background = fondoNiveles[8];
                            break;

                        case 4:
                            level5.Click -= Level;
                            level5.Background = fondoNiveles[8];
                            break;

                        case 5:
                            level6.Click -= Level;
                            level6.Background = fondoNiveles[8];
                            break;

                        case 6:
                            level7.Click -= Level;
                            level7.Background = fondoNiveles[8];
                            break;

                        case 7:
                            level8.Click -= Level;
                            level8.Background = fondoNiveles[8];
                            break;
                    }
                }
                if (Desbloqueado[i] == true)
                {
                    switch (i)
                    {
                        case 0:
                            if (level1.Background == fondoNiveles[8])
                            {
                                level1.Click += Level;
                                level1.Background = fondoNiveles[0];
                            }
                            break;

                        case 1:
                            if (level2.Background == fondoNiveles[8])
                            {
                                level2.Click += Level;
                                level2.Background = fondoNiveles[1];
                            }
                            break;

                        case 2:
                            if (level3.Background == fondoNiveles[8])
                            {
                                level3.Click += Level;
                                level3.Background = fondoNiveles[2];
                            }
                            break;

                        case 3:
                            if (level4.Background == fondoNiveles[8])
                            {
                                level4.Click += Level;
                                level4.Background = fondoNiveles[3];
                            }
                            break;

                        case 4:
                            if (level5.Background == fondoNiveles[8])
                            {
                                level5.Click += Level;
                                level5.Background = fondoNiveles[4];
                            }
                            break;

                        case 5:
                            if (level6.Background == fondoNiveles[8])
                            {
                                level6.Click += Level;
                                level6.Background = fondoNiveles[5];
                            }
                            break;

                        case 6:
                            if (level7.Background == fondoNiveles[8])
                            {
                                level7.Click += Level;
                                level7.Background = fondoNiveles[6];
                            }
                            break;

                        case 7:
                            if (level8.Background == fondoNiveles[8])
                            {
                                level8.Click += Level;
                                level8.Background = fondoNiveles[7];
                            }
                            break;
                    }
                }
            }
        }
        #endregion

        #region Teclas
        private void Key_Down(object sender, KeyEventArgs e)
        {
            switch(e.Key) 
            {
                case Key.Escape:
                    MainWindow m = new MainWindow();
                    m.Show();
                    this.Close();
                    break;
            }
        }

        #endregion

        #endregion
    }
}
