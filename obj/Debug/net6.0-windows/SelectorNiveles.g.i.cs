﻿#pragma checksum "..\..\..\SelectorNiveles.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "539EE31C5BC82098A44448BD325182D65747F0E6"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using Doki_doki_tetris_club;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Doki_doki_tetris_club {
    
    
    /// <summary>
    /// SelectorNiveles
    /// </summary>
    public partial class SelectorNiveles : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 423 "..\..\..\SelectorNiveles.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button level1;
        
        #line default
        #line hidden
        
        
        #line 426 "..\..\..\SelectorNiveles.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button level2;
        
        #line default
        #line hidden
        
        
        #line 429 "..\..\..\SelectorNiveles.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button level3;
        
        #line default
        #line hidden
        
        
        #line 432 "..\..\..\SelectorNiveles.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button level4;
        
        #line default
        #line hidden
        
        
        #line 435 "..\..\..\SelectorNiveles.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button level5;
        
        #line default
        #line hidden
        
        
        #line 438 "..\..\..\SelectorNiveles.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button level6;
        
        #line default
        #line hidden
        
        
        #line 441 "..\..\..\SelectorNiveles.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button level7;
        
        #line default
        #line hidden
        
        
        #line 444 "..\..\..\SelectorNiveles.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button level8;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "7.0.2.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Doki doki tetris club;component/selectorniveles.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\SelectorNiveles.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "7.0.2.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\..\SelectorNiveles.xaml"
            ((Doki_doki_tetris_club.SelectorNiveles)(target)).KeyDown += new System.Windows.Input.KeyEventHandler(this.Key_Down);
            
            #line default
            #line hidden
            return;
            case 2:
            this.level1 = ((System.Windows.Controls.Button)(target));
            
            #line 423 "..\..\..\SelectorNiveles.xaml"
            this.level1.Click += new System.Windows.RoutedEventHandler(this.Level);
            
            #line default
            #line hidden
            return;
            case 3:
            this.level2 = ((System.Windows.Controls.Button)(target));
            
            #line 426 "..\..\..\SelectorNiveles.xaml"
            this.level2.Click += new System.Windows.RoutedEventHandler(this.Level);
            
            #line default
            #line hidden
            return;
            case 4:
            this.level3 = ((System.Windows.Controls.Button)(target));
            
            #line 429 "..\..\..\SelectorNiveles.xaml"
            this.level3.Click += new System.Windows.RoutedEventHandler(this.Level);
            
            #line default
            #line hidden
            return;
            case 5:
            this.level4 = ((System.Windows.Controls.Button)(target));
            
            #line 432 "..\..\..\SelectorNiveles.xaml"
            this.level4.Click += new System.Windows.RoutedEventHandler(this.Level);
            
            #line default
            #line hidden
            return;
            case 6:
            this.level5 = ((System.Windows.Controls.Button)(target));
            
            #line 435 "..\..\..\SelectorNiveles.xaml"
            this.level5.Click += new System.Windows.RoutedEventHandler(this.Level);
            
            #line default
            #line hidden
            return;
            case 7:
            this.level6 = ((System.Windows.Controls.Button)(target));
            
            #line 438 "..\..\..\SelectorNiveles.xaml"
            this.level6.Click += new System.Windows.RoutedEventHandler(this.Level);
            
            #line default
            #line hidden
            return;
            case 8:
            this.level7 = ((System.Windows.Controls.Button)(target));
            
            #line 441 "..\..\..\SelectorNiveles.xaml"
            this.level7.Click += new System.Windows.RoutedEventHandler(this.Level);
            
            #line default
            #line hidden
            return;
            case 9:
            this.level8 = ((System.Windows.Controls.Button)(target));
            
            #line 444 "..\..\..\SelectorNiveles.xaml"
            this.level8.Click += new System.Windows.RoutedEventHandler(this.Level);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

