﻿using Microsoft.Win32;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Doki_doki_tetris_club
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Atributos

        private int Level;

        private DispatcherTimer tiempo = new DispatcherTimer();

        private const string MenuMusic = "Recursos/Ost/MainMenu.wav";

        private WaveStream streamMenuMusic;
        private WaveOut PlayMenuMusic;
        #endregion

        #region Constructor
        public MainWindow()
        {
            InitializeComponent();
            streamMenuMusic = new WaveFileReader(@MenuMusic);
            PlayMenuMusic = new WaveOut();
            PlayMenuMusic.Init(streamMenuMusic);
            PlayMenuMusic.Play();
            tiempo.Interval = TimeSpan.FromSeconds(1);
            tiempo.Start();
            tiempo.Tick += timer_Tick;
            Level = Comprobar();
            if (Level == 0) 
            {
                Continuar.Foreground = new SolidColorBrush(Color.FromRgb(105, 105, 105));
            }
        }
        #endregion

        #region Métodos

        #region Eventos click
        private void Label_MouseLeftButtonDown
            (object sender, MouseButtonEventArgs e)
        {
            PlayMenuMusic.Stop();
            Opciones op = new Opciones();
            op.Show();
            this.Close();
        }

        private void Juego_Click
            (object sender, MouseButtonEventArgs e)
        {
            PlayMenuMusic.Stop();
            PantallaJuego P = new PantallaJuego(0);
            P.Show();
            this.Close();
        }

        private void Salir_click(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Nuevo_click(object sender, MouseButtonEventArgs e)
        {
            PlayMenuMusic.Stop();
            Video v = new Video();
            v.Show();
            this.Close();
        }

        private void Extras_click(object sender, MouseButtonEventArgs e)
        {
            PlayMenuMusic.Stop();
            SelectorNiveles s = new SelectorNiveles();
            s.Show();
            this.Close();
        }

        private void Continue(object sender, MouseButtonEventArgs e)
        {   
            Level = Comprobar();

            if (Level != 0 && Level != 8)
            {
                PlayMenuMusic.Stop();
                PantallaJuego p = new PantallaJuego(Level + 1);
                p.Show();
                this.Close();
            }
        }
        #endregion

        #region Música
        private void reiniciarMusica()
        {
            if (streamMenuMusic.Position == 24524732)
            {
                PlayMenuMusic.Stop();
                streamMenuMusic.Position = 0;
                PlayMenuMusic.Play();
            }
        }
        #endregion

        #region Temporizador
        private void timer_Tick(object sender, EventArgs e)
        {
            reiniciarMusica();
        }
        #endregion

        #region Comprobación

        public int Comprobar()
        {
            string Continuar = new string(@"../../../Recursos/Ficheros/Continuar.txt");
            string linea;
            int level = 0;

            if (File.Exists(Continuar))
            {
                using (StreamReader RContinue = new StreamReader(Continuar))
                {
                    while ((linea = RContinue.ReadLine()) != null)
                    {
                        switch(linea)
                        {
                            case "Level1":
                                level = 1;
                                break;
                            case "Level2":
                                level = 2;
                                break;
                            case "Level3":
                                level = 3;
                                break;
                            case "Level4":
                                level = 4;
                                break;
                            case "Level5":
                                level = 5;
                                break;
                            case "Level6":
                                level = 6;
                                break;
                            case "Level7":
                                level = 7;
                                break;
                            case "Level8":
                                level = 8;
                                break;
                        }
                    }
                }
            }
            return level;
        }
        #endregion

        #endregion
    }
}
