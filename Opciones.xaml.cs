﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Doki_doki_tetris_club
{
    /// <summary>
    /// Lógica de interacción para Opciones.xaml
    /// </summary>
    public partial class Opciones : Window
    {
        #region Atributos
        private const string OptionMusic = "Recursos/Ost/OptionMusic.wav";

        private WaveStream streamOptionMusic;

        private WaveOut PlayOptionMusic;
        #endregion

        #region Constructor
        public Opciones()
        {
            InitializeComponent();
            streamOptionMusic = new WaveFileReader(@OptionMusic);
            PlayOptionMusic = new WaveOut();
            PlayOptionMusic.Init(streamOptionMusic);
            PlayOptionMusic.Play();
        }
        #endregion

        #region Métodos

        #region Eventos click
        private void Label_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            PlayOptionMusic.Stop();
            MainWindow mainWindow = new MainWindow();
            this.Close();
            mainWindow.Show();
        }
        #endregion

        #region Slider
        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }
        #endregion

        #region Música
        private void reiniciarMusica(object sender, System.EventArgs e)
        {
            if (streamOptionMusic.Position == streamOptionMusic.Length)
            {
                streamOptionMusic.Position = 0;
                PlayOptionMusic.Play();
            }
        }
        #endregion

        #endregion
    }
}
